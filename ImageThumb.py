from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from RawImage import *

DOT_SIZE = 50

class ImageThumb (QGraphicsPixmapItem):
  # image: RawImage
  def __init__(self, image):
    super(ImageThumb, self).__init__()
    pixmap = image.getPixmap()
    self.setPixmap(pixmap)
    # BBox
    x, y = image.getBoundingBoxCenter()
    bbcenter = QGraphicsEllipseItem(x - DOT_SIZE / 2, y - DOT_SIZE / 2, DOT_SIZE, DOT_SIZE, self)
    bbcenter.setBrush(QBrush(Qt.blue))
    # contour centroid
    x, y = image.getContourCentroid()
    ccentroid = QGraphicsEllipseItem(x - DOT_SIZE / 2, y - DOT_SIZE / 2, DOT_SIZE, DOT_SIZE, self)
    ccentroid.setBrush(QBrush(Qt.darkYellow))
    # convex hull centroid
    x, y = image.getConvexHullCentroid()
    chcentroid = QGraphicsEllipseItem(x - DOT_SIZE / 2, y - DOT_SIZE / 2, DOT_SIZE, DOT_SIZE, self)
    chcentroid.setBrush(QBrush(Qt.green))
    # weighted centroid (all pixels)
    x, y = image.getWeightedCentroid()
    wcentroid = QGraphicsEllipseItem(x - DOT_SIZE / 2, y - DOT_SIZE / 2, DOT_SIZE, DOT_SIZE, self)
    wcentroid.setBrush(QBrush(Qt.yellow))
    # minimum enclosing circle
    x, y = image.getMinEnclosingCircle()
    ecircle = QGraphicsEllipseItem(x - DOT_SIZE / 2, y - DOT_SIZE / 2, DOT_SIZE, DOT_SIZE, self)
    ecircle.setBrush(QBrush(Qt.magenta))
    # minimum area rectangle
    x, y = image.getMinAreaRectangle()
    rect = QGraphicsEllipseItem(x - DOT_SIZE / 2, y - DOT_SIZE / 2, DOT_SIZE, DOT_SIZE, self)
    rect.setBrush(QBrush(Qt.cyan))
    # fitting ellipse
    x, y = image.getFittingEllipse()
    ellipse = QGraphicsEllipseItem(x - DOT_SIZE / 2, y - DOT_SIZE / 2, DOT_SIZE, DOT_SIZE, self)
    ellipse.setBrush(QBrush(Qt.darkCyan))
    # pole of inaccessibility centroid
    x, y = image.getPole()
    pole = QGraphicsEllipseItem(x - DOT_SIZE / 2, y - DOT_SIZE / 2, DOT_SIZE, DOT_SIZE, self)
    pole.setBrush(QBrush(Qt.red))
