import sys

from PyQt5 import QtGui
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from ShapeView import *

def tr(str):
  return str

class ShapeMainWindow (QMainWindow): 
  def __init__(self):
    super(ShapeMainWindow,self).__init__()

    bar = self.menuBar()
    fileMenu = bar.addMenu("File")
    openAct = QAction("Open...", self)
    openAct.setShortcut("Ctrl+O")
    openAct.setToolTip(tr("Open image"))
    openAct.setStatusTip(tr("Open image"))
    openAct.triggered.connect(self.open)
    fileMenu.addAction(openAct)

    scene = QGraphicsScene()
    self.view = ShapeView(scene)
    self.setCentralWidget(self.view)
    self.view.errorMessage.connect(self.displayMessage)

    self.messages = QTextEdit()
    self.messages.setReadOnly(True)
    messageview = QDockWidget(self)
    messageview.setWindowTitle('Messages')
    messageview.setAllowedAreas(Qt.BottomDockWidgetArea)
    messageview.setWidget(self.messages)
    messageview.setFeatures(QDockWidget.NoDockWidgetFeatures)
    self.addDockWidget(Qt.BottomDockWidgetArea, messageview)

    self.resize(800, 600)


  def open(self):
    dlg = QFileDialog()
    dlg.setFileMode(QFileDialog.AnyFile)
    dlg.setAcceptMode(QFileDialog.AcceptOpen)
    dlg.setNameFilter("Image Files (*.png *.jpg *.bmp)")

    filenames, _ = dlg.getOpenFileNames(self, "Open images", ".", "Image Files (*.png *.jpg *.bmp)")
    for i in range(len(filenames)):
        self.view.open(filenames[i])

  def displayMessage(self, message):
    self.messages.append(message)

if __name__ == "__main__": 
  app = QApplication(sys.argv)
  win = ShapeMainWindow()
  win.show()
  sys.exit(app.exec_())