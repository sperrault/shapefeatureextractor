# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How to use? ###
python ShapeFeatureExtractor.py
Runs with Python 2, requires OpenCV 2

### Input ###
The code will automatically looks for any jpg file in the **img** folder (extension must be .jpg).
Before any processing is done, the image will be converted to a greyscale image and the contours will be extracted. If there are more than 1 contour area, the code will dilate the image, one iteration at a time, until there is only one contour area left.

### Output ###
Each processed image will generate 9 different jpeg files (for each different feature). A CSV also summarizes the findings.
