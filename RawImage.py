from PyQt5.QtGui import *

import numpy as np
import cv2

BW_THRESHOLD = 240

class UnsupportedFormat(Exception):
  pass

class NoContour(Exception):
  pass

class MoreThanOneContour(Exception):
  pass

class NoBlackPixel(Exception):
  pass

class NotEnoughPoints(Exception):
  pass

class RawImage:
  def __init__(self, fileName):
    self.image = cv2.imread(fileName, cv2.IMREAD_COLOR)
    self.height, self.width, _ = self.image.shape

    #get thresholded image
    if (len(self.image.shape) == 3):
        image_nb = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
    else:
        image_nb = self.image
    _, self.binary_image = cv2.threshold(image_nb, BW_THRESHOLD, 255, cv2.THRESH_BINARY_INV)

    #get contours /!\ when nb > 1
    contours,_ = cv2.findContours(self.binary_image, cv2.RETR_EXTERNAL, 1)

    if len(contours) == 0:
      raise NoContour
    if len(contours) > 1:
      raise MoreThanOneContour
    self.contour = contours[0]

  def getImage(self):
    return self.image

  def getPixmap(self):
    qformat = QImage.Format_Indexed8
    if len(self.image.shape) == 3:
      if self.image.shape[2] == 4:
        qformat = QImage.Format_RGBA8888
      else:
        qformat = QImage.Format_RGB888
      img = QImage(self.image.data, self.image.shape[1], self.image.shape[0], self.image.strides[0], qformat)
      img = img.rgbSwapped()
      pixmap = QPixmap.fromImage(img)
      return pixmap
    else:
      raise UnsupportedFormat

  def getBoundingBoxCenter(self):
    x, y, w, h = cv2.boundingRect(self.contour)
    cX = x + (w / 2)
    cY = y + (h / 2)
    return cX, cY

  def getContourCentroid(self):
    rows, cols, _ = self.image.shape
    im = np.zeros((rows,cols), np.uint8)
    cv2.drawContours(im, [self.contour], -1, (255,255,255), 1)
    cX = 0.0
    cY = 0.0
    count = 0
    for ii in range(rows):
      for jj in range(cols):
        k = im[ii, jj]
        if (k > BW_THRESHOLD):
          count += 1
          cY += ii
          cX += jj
    if count == 0:
      raise NoBlackPixel
    cX = cX / count
    cY = cY / count
    return cX, cY

  def getConvexHullCentroid(self):
    rows, cols, _ = self.image.shape
    im = np.zeros((rows,cols), np.uint8)
    hull = cv2.convexHull(self.contour)
    filled = np.zeros((rows, cols), np.uint8)
    cv2.drawContours(filled, [hull], 0, (255,255,255), -1)
    cX = 0.0
    cY = 0.0
    count = 0
    for ii in range(rows):
      for jj in range(cols):
        k = filled[ii, jj]
        if(k > BW_THRESHOLD):
          count += 1
          cY += ii
          cX += jj
    cX = cX / count
    cY = cY / count
    return cX, cY

  def getWeightedCentroid(self):
    rows, cols, _ = self.image.shape
    cX = 0.0
    cY = 0.0
    count = 0
    for ii in range(rows):
      for jj in range(cols):
        k = self.binary_image[ii, jj]
        if(k > BW_THRESHOLD):
          count += 1
          cY += ii
          cX += jj
    if count == 0:
      raise NoBlackPixel
    cX = cX / count
    cY = cY / count
    return cX, cY

  def getMinEnclosingCircle(self):
    (cX, cY), _ = cv2.minEnclosingCircle(self.contour)
    return cX, cY

  def getMinAreaRectangle(self):
    rect = cv2.minAreaRect(self.contour)
    (x1, y1), _, (x2, y2), _ = cv2.boxPoints(rect)
    cX = x1 + (x2 - x1) / 2
    cY = y1 + (y2 - y1) / 2
    return cX, cY

  def getFittingEllipse(self):
    if len(self.contour) < 5:
      raise NotEnoughPoints
    (cX, cY), _, _ = cv2.fitEllipse(self.contour)
    return cX, cY

  def getPole(self):
    rows, cols, _ = self.image.shape
    im = np.zeros((rows,cols), np.uint8)
    cv2.drawContours(im, [self.contour], 0, (255,255,255), cv2.FILLED)
    # avoid tore
    cv2.rectangle(im, (0,0), (cols,rows), (0, 0, 0), 2)
    dist = cv2.distanceTransform(im, cv2.DIST_L2, 5, cv2.DIST_LABEL_PIXEL)
    maxv = np.amax(dist)
    _, thresh = cv2.threshold(dist, maxv-1, 255, cv2.THRESH_BINARY)
    cX = 0.0
    cY = 0.0
    count = 0
    for ii in range(rows):
      for jj in range(cols):
        k = thresh[ii,jj]
        if (k > BW_THRESHOLD):
          count += 1
          cY += ii
          cX += jj
    if count == 0:
      raise NoBlackPixel
    cX = cX/count
    cY = cY/count
    return cX, cY
