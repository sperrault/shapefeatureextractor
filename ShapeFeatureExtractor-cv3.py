import numpy as np
from numpy  import array
import cv2
import glob
import csv
import os

BW_THRESHOLD = 240

DETAILSTHRESHOLD = 20

THICKNESS = 5

DISTANCE_THRESHOLD = 50

MAXCONNECTEDCOMPDIST = 60

SIMPLIFIED = False

def resize_image(image, coefficient):
    return cv2.resize(image, (coefficient, coefficient))

def get_image_contour(image):
    #thresh = cv2.equalizeHist(image)
    if (len(img.shape) == 3):
        image_nb = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    else:
        image_nb = image
    ret, thresh = cv2.threshold(image_nb, BW_THRESHOLD, 255, cv2.THRESH_BINARY_INV)
    #kernel = np.ones((5,5),np.uint8)
    kernel = array([[0,1,0],[1,1,1],[0,1,0]], dtype=np.uint8)
    #eliminate details
    if (SIMPLIFIED):
        thresh = cv2.erode(thresh,kernel,iterations = DETAILSTHRESHOLD)
        thresh = cv2.dilate(thresh,kernel,iterations = 2*DETAILSTHRESHOLD)
        thresh = cv2.erode(thresh,kernel,iterations = DETAILSTHRESHOLD)
    #try to get only one contour
    contours,_ = cv2.findContours(thresh,cv2.RETR_EXTERNAL,1)
    iteration = 0
    if (len(contours) > 1):
        print('More than one contour area found. Merging contour areas...')
        for it in range(1, MAXCONNECTEDCOMPDIST):
            thresh = cv2.dilate(thresh,kernel,iterations = it)
            thresh = cv2.erode(thresh,kernel,iterations = it)
            contours,_ = cv2.findContours(thresh,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            if len(contours) == 1:
                iteration = it
                break
    return [thresh, contours, iteration]

def get_processed_contour_as_image(image, contour):
    width, height = image.shape[:2]
    im = np.zeros((width, height, 3), np.uint8)
    cv2.drawContours(im,contours,-1,(255,255,255),-1)
    im = cv2.bitwise_not(im)
    return im

#DONE
def get_contours_centroid(image, imageout, contour):
    img = imageout.copy()
    im = np.zeros(image.shape, np.uint8)
    cv2.drawContours(im,[contour],-1,(255,255,255),1)
    #M = cv2.moments(contour)
    #cX = M['m10']/M['m00']
    #cY = M['m01']/M['m00']
    rows,cols = im.shape
    cX = 0.0
    cY = 0.0
    count = 0
    for ii in range(rows):
        for jj in range(cols):
            k = im[ii,jj]
            if(k>BW_THRESHOLD):
                count += 1
                cY += ii
                cX += jj
    if count == 0:
        print("Empty blob: adjust the threshold")
    cX = cX/count
    cY = cY/count
    cv2.drawContours(img,[contour],-1,(0,255,0),THICKNESS)
    cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], THICKNESS)
    return ["{},{}".format(cX,cY),img]

#DONE
def get_bounding_box(image, imageout, contour):
    img = imageout.copy()
    x,y,w,h = cv2.boundingRect(contour)
    cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),THICKNESS)
    cX = x+(w/2)
    cY = y+(h/2)
    cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], THICKNESS)
    #cv2.imshow('Bounding Rectangle',img)
    return ["{},{}".format(cX,cY),img]

#DONE
def get_min_area_rectangle(image, imageout, contour):
    img = imageout.copy()
    rect = cv2.minAreaRect(contour)
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    (x1,y1),_,(x2,y2),_ = cv2.boxPoints(rect)
    cX = x1 + (x2-x1)/2
    cY = y1 + (y2-y1)/2
    cv2.drawContours(img, [box], 0, (0,255,0), THICKNESS)
    cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], THICKNESS)
    #cv2.imshow('minAreaRectangle',img)
    return ["{},{}".format(cX,cY),img]

#DONE
def get_convex_hull(image, imageout, contour):
    img = imageout.copy()
    hull = cv2.convexHull(contour)
    filled = np.zeros(image.shape,np.uint8)
    cv2.drawContours(filled, [hull], 0, (255,255,255), -1)
    rows,cols = filled.shape
    cX = 0.0
    cY = 0.0
    count = 0
    for ii in range(rows):
        for jj in range(cols):
            k = filled[ii,jj]
            if(k>250):
                count += 1
                cY += ii
                cX += jj
    cX = cX/count
    cY = cY/count
    '''
    print "hull size:" + format(len(hull))
    for j in range(0,len(hull)):
        cX += hull[j][0][0]
        cY += hull[j][0][1]
        print "    point " + format(hull[j][0][0]) + "," + format(hull[j][0][1])
    cX = cX/len(hull)
    cY = cY/len(hull)
    '''
    hull2 = cv2.convexHull(contour, returnPoints = False)
    defects = cv2.convexityDefects(contour,hull2)

    if defects is not None:
        for i in range(defects.shape[0]):
            s,e,f,d = defects[i,0]
            start = tuple(contour[s][0])
            end = tuple(contour[e][0])
            far = tuple(contour[f][0])
            cv2.line(img,start,end,[0,255,0],THICKNESS)
            cv2.circle(img,far,1,[100,150,255],THICKNESS)
            cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], THICKNESS)
    #cv2.imshow('Hull',img)
    return ["{},{}".format(cX,cY),img]

#DONE
def get_ellipse(image, imageout,contour):
    img = imageout.copy()
    ellipse = cv2.fitEllipse(contour)
    cv2.ellipse(img,ellipse,(0,255,0),THICKNESS)
    (cX,cY),_,_ = cv2.fitEllipse(contour)
    cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], THICKNESS)
    #cv2.imshow('Ellipse',img)
    return ["{},{}".format(cX,cY),img]

#def is_blank(image):
#    rows,cols = image.shape
#    for ii in range(rows):
#        for jj in range(cols):
#            if(k > BW_THRESHOLD):
#                return True
#    return False

#DONE
def get_pole(image, imageout,contour):
    rows, cols = image.shape
    img = imageout.copy()
    im = np.zeros(image.shape, np.uint8)
    cv2.drawContours(im, [contour], 0, (255,255,255), cv2.FILLED)
    # avoid tore
    cv2.rectangle(im, (0,0), (cols,rows), (0, 0, 0), 2)
    dist = cv2.distanceTransform(im, cv2.DIST_L2, 5, cv2.DIST_LABEL_PIXEL)
    maxv = np.amax(dist)
    _, thresh = cv2.threshold(dist, maxv-1, 255, cv2.THRESH_BINARY)
    cX = 0.0
    cY = 0.0
    count = 0
    for ii in range(rows):
        for jj in range(cols):
            k = thresh[ii,jj]
            if (k > BW_THRESHOLD):
                count += 1
                cY += ii
                cX += jj
                img[ii,jj] = (0, 255, 255)
    if count == 0:
        print("Empty blob: adjust the threshold")
    cX = cX/count
    cY = cY/count
    cv2.circle(img,(int(cX),int(cY)),int(maxv),(0,255,0),2)
    cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], THICKNESS)
    return ["{},{}".format(cX,cY),img]

#DONE
def get_min_enclosing_circle(image, imageout,contour):
    img = imageout.copy()
    (cX,cY),radius = cv2.minEnclosingCircle(contour)
    cv2.circle(img,(int(cX),int(cY)),int(radius),(0,255,0),THICKNESS)
    #cv2.drawContours(img, [contour], 0, (255,255,0), 4)
    cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], THICKNESS)
    #cv2.imshow('MinEnclosingCircle',img)
    return ["{},{}".format(int(cX),int(cY)),img]

#DONE
def get_weighted_centroid(image, imageout,contour):
    img = imageout.copy()
    rows,cols = image.shape
    cX = 0.0
    cY = 0.0
    count = 0
    for ii in range(rows):
        for jj in range(cols):
            k = image[ii,jj]
            if(k>BW_THRESHOLD):
                count += 1
                cY += ii
                cX += jj
    if count == 0:
        print("Empty blob: adjust the threshold")
    cX = cX/count
    cY = cY/count
    cv2.drawContours(img,[contour],-1,(0,255,0),THICKNESS)
    cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], THICKNESS)
    #cv2.imshow('Weighted Density',img)
    return ["{},{}".format(cX,cY),img]
'''
def get_eroded(image, imageout):
    img = imageout.copy()
    img_gray = img
    if (len(img.shape) == 3):
        #print 'Converting from Color to Greyscale'
        img_gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(img_gray, BW_THRESHOLD, 255, 0)
    thresh = cv2.bitwise_not(thresh)
    eroded = cv2.erode(thresh, None, iterations=3)
    im,contours,hierarchy = cv2.findContours(eroded,2,1)
    eroded_cnts = contours[0]
    if (len(eroded_cnts)>0):
        M = cv2.moments(eroded_cnts)
        cX = M["m10"] / M["m00"]
        cY = M["m01"] / M["m00"]
        cv2.circle(img,(int(cX),int(cY)),2,[255,0,0], -1)
        #cv2.imshow('Eroded',img)
        return ["{},{}".format(cX,cY),img]
    else:
        return [",",img]

def get_dilated(image, imageout):
    img = imageout.copy()
    img_gray = img
    if (len(img.shape) == 3):
        #print 'Converting from Color to Greyscale'
        img_gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(img_gray, BW_THRESHOLD, 255, 0)
    thresh = cv2.bitwise_not(thresh)
    dilated = cv2.dilate(thresh, None, iterations=3)
    im,contours,hierarchy = cv2.findContours(dilated,2,1)
    dilated_cnts = contours[0]
    if (len(dilated_cnts)>0):
        M = cv2.moments(dilated_cnts)
        cX = M["m10"] / M["m00"]
        cY = M["m01"] / M["m00"]
        cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], -1)
        #cv2.imshow('Dilated',img)
        return ["{},{}".format(cX,cY),img]
    else:
        return ","
'''
directory = os.path.dirname("output/")
lowest_size = 10000.0
try:
    os.stat(directory)
except:
    print("Generating output directory")
    os.mkdir(directory)
with open('output.csv', 'w') as outputfile:
    outputfile.write("FileName,Iterations,ContoursCenterX,ContoursCenterY,BoundingBoxX,BoundingBoxY,MinAreaRectX,MinAreaRectY,HullX,HullY,EllipseX,EllipseY,WeightedDensityX,WeightedDensityY,MinEnclosingCircleX,MinEnclosingCircleY,PoleInaccessibilityX,PoleInaccessibilityY\n")
    image_list = []
    for filename in glob.glob('img/*.jpg'):
        current = cv2.imread(filename,cv2.IMREAD_COLOR)
        height, _, _ = current.shape
        if height < lowest_size:
            lowest_size = height
    #print('Smallest icon has size of {} pixels.\nAll images will be scaled to this size.'.format(lowest_size))
    for filename in sorted(glob.glob('img/*.jpg')):
    #for filename in ["img/1.jpg", "img/placeholder-2.jpg", "img/document.jpg", "img/notebook-4.jpg"]:
        print("\033[92mProcessing " + filename + "...\033[0m")
        name_base = os.path.basename(filename)[:-4]
        name_base = "output/"+name_base
        img = cv2.imread(filename,cv2.IMREAD_GRAYSCALE)
        img1 = resize_image(img,lowest_size)
        img1,contours,iterations = get_image_contour(img1)
        #print "Contour length: ", len(contours[0])
        if len(contours) == 0:
            print("\033[31mNo contour, skipping this picture\033[0m")
            continue
        if len(contours) > 1:
            print("\033[31mSeveral connected components, skipping this picture\033[0m")
            continue
        if len(contours[0]) < 5:
            print("\033[31mNeed at least 5 points for ellipse fit, skipping this picture\033[0m")
            continue
        #img2 = resize_image(outimg,lowest_size)
        img2 = get_processed_contour_as_image(img1, contours)
        cv2.imwrite(name_base+"_original.jpg", img1)
        cv2.imwrite(name_base+"_processed.jpg", img2)
        myRow = ""
        myRow += "{},".format(name_base)
        cont = get_contours_centroid(img1, img2, contours[0])
        myRow += "{},".format(iterations)
        cv2.imwrite(name_base+"_contours.jpg", cont[1])
        myRow += "{},".format(cont[0])
        bBox = get_bounding_box(img1, img2, contours[0])
        myRow += "{},".format(bBox[0])
        cv2.imwrite(name_base+"_bounding.jpg", bBox[1])
        minRect = get_min_area_rectangle(img1, img2, contours[0])
        myRow += "{},".format(minRect[0])
        cv2.imwrite(name_base+"_minAreaRect.jpg", minRect[1])
        hull = get_convex_hull(img1, img2, contours[0])
        myRow += "{},".format(hull[0])
        cv2.imwrite(name_base+"_hull.jpg", hull[1])
        ellipse = get_ellipse(img1, img2, contours[0])
        myRow += "{},".format(ellipse[0])
        cv2.imwrite(name_base+"_ellipse.jpg", ellipse[1])
        density = get_weighted_centroid(img1, img2, contours[0])
        myRow += "{},".format(density[0])
        cv2.imwrite(name_base+"_weighted.jpg", density[1])
        minCircle = get_min_enclosing_circle(img1, img2, contours[0])
        myRow += "{},".format(minCircle[0])
        cv2.imwrite(name_base+"_minCircle.jpg", minCircle[1])
        pole = get_pole(img1, img2, contours[0])
        myRow += "{}".format(pole[0])
        cv2.imwrite(name_base+"_pole.jpg", pole[1])
        #erod = get_eroded(img1)
        #myRow += "{},".format(erod[0])
        #cv2.imwrite(name_base+"_eroded.jpg", erod[1])
        #dilat = get_dilated(img1)
        #myRow +=  "{},".format(dilat[0])
        #cv2.imwrite(name_base+"_dilated.jpg", dilat[1])
        myRow += "\n"
        outputfile.write(myRow)

