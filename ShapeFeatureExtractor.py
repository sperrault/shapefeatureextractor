import numpy as np
from numpy  import array
import cv2
import glob
import csv
import os

BW_THRESHOLD = 200

THICKNESS = 5

def resize_image(image, coefficient):
    return cv2.resize(image, (coefficient, coefficient))

DISTANCE_THRESHOLD = 180

def find_if_close(cnt1,cnt2):
    row1,row2 = cnt1.shape[0],cnt2.shape[0]
    for i in xrange(row1):
        for j in xrange(row2):
            dist = np.linalg.norm(cnt1[i]-cnt2[j])
            if abs(dist) < DISTANCE_THRESHOLD :
                return True
            elif i==row1-1 and j==row2-1:
                return False

def get_image_contour(image):
    img_gray = image
    if (len(image.shape) == 3):
        img_gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(img_gray, BW_THRESHOLD, 255, 0)
    thresh = cv2.bitwise_not(thresh)
    contours,hierarchy = cv2.findContours(thresh,cv2.RETR_EXTERNAL,1)
    #contours = sorted(contours, key=cv2.contourArea, reverse=True)
    iteration = 0
    LENGTH = len(contours)
    if (LENGTH > 1):
        print 'More than one contour area found. Merging contour areas...'
        iteration = 1
        status = np.zeros((LENGTH,1))

        for i,cnt1 in enumerate(contours):
            x = i    
            if i != LENGTH-1:
                for j,cnt2 in enumerate(contours[i+1:]):
                    x = x+1
                    dist = find_if_close(cnt1,cnt2)
                    if dist == True:
                        val = min(status[i],status[x])
                        status[x] = status[i] = val
                    else:
                        if status[x]==status[i]:
                            status[x] = i+1

        unified = []
        maximum = int(status.max())+1
        for i in xrange(maximum):
            pos = np.where(status==i)[0]
            if pos.size != 0:
                cont = np.vstack(contours[i] for i in pos)
                hull = cv2.convexHull(cont)
                unified.append(hull)
        contours = unified
    return [contours, iteration]

#def get_image_contour(image):
#    img_gray = image
#    if (len(image.shape) == 3):
#        img_gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
#    ret, thresh = cv2.threshold(img_gray, BW_THRESHOLD, 255, 0)
#    thresh = cv2.bitwise_not(thresh)
#    contours,hierarchy = cv2.findContours(thresh,2,1)
#    contours = sorted(contours, key=cv2.contourArea, reverse=True)
#    iteration = 0
#    #while len(contours) > 1:
#    #    thresh = cv2.dilate(thresh, None, iterations=1)
#    #    iteration = iteration + 1
#    #    contours,hierarchy = cv2.findContours(thresh,2,1)
#    return [contours, iteration]

def get_processed_contour_as_image(image):
    contours, _ = get_image_contour(image)
    img1width, img1height = image.shape[:2]
    emptyImage = np.zeros((img1width, img1height, 3), np.uint8)
    cv2.drawContours(emptyImage,contours,-1,(255,255,255),-1)
    emptyImage = cv2.bitwise_not(emptyImage)
    return emptyImage

def get_contours_centroid(image):
    img = image.copy()
    contours = get_image_contour(img)[0]
    c = contours[0]
    M = cv2.moments(c)
    cX = M['m10']/M['m00']
    cY = M['m01']/M['m00']
    cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], -1)
    return ["{},{}".format(cX,cY),img]

def get_bounding_box(image):
    img = image.copy()
    contours = get_image_contour(image)[0]
    c = contours[0]
    x,y,w,h = cv2.boundingRect(c)
    cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),THICKNESS)
    cX = x+(w/2)
    cY = y+(h/2)
    cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], -1)
    #cv2.imshow('Bounding Rectangle',img)
    return ["{},{}".format(cX,cY),img]

def get_min_area_rectangle(image):
    img = image.copy()
    contours = get_image_contour(image)[0]
    c = contours[0]
    rect = cv2.minAreaRect(c)
    box = cv2.cv.BoxPoints(rect)
    box = np.int0(box)
    (x1,y1),_,(x2,y2),_ = cv2.cv.BoxPoints(rect)
    cX = x1 + (x2-x1)/2
    cY = y1 + (y2-y1)/2
    cv2.drawContours(img, [box], 0, (0,255,0), THICKNESS)
    cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], -1)
    #cv2.imshow('minAreaRectangle',img)
    return ["{},{}".format(cX,cY),img]

def get_convex_hull(image):
    img = image.copy()
    contours = get_image_contour(image)[0]
    cnt = contours[0]
    hull = cv2.convexHull(cnt)
    filled = np.zeros(img.shape,np.uint8)
    cv2.drawContours(filled, [hull], 0, (255,255,255), -1)
    rows,cols,layers = filled.shape
    cX = 0.0;
    cY = 0.0;
    count = 0
    for ii in range(rows):
        for jj in range(cols):
            k = filled[ii,jj,0]
            if(k>250):
                count += 1
                cY += ii
                cX += jj
    cX = cX/count
    cY = cY/count
    '''
    print "hull size:" + format(len(hull))
    for j in range(0,len(hull)):
        cX += hull[j][0][0]
        cY += hull[j][0][1]
        print "    point " + format(hull[j][0][0]) + "," + format(hull[j][0][1])
    cX = cX/len(hull)
    cY = cY/len(hull)
    '''
    hull2 = cv2.convexHull(cnt, returnPoints = False)
    defects = cv2.convexityDefects(cnt,hull2)

    if defects is not None:
        for i in range(defects.shape[0]):
            s,e,f,d = defects[i,0]
            start = tuple(cnt[s][0])
            end = tuple(cnt[e][0])
            far = tuple(cnt[f][0])
            cv2.line(img,start,end,[0,255,0],THICKNESS)
            cv2.circle(img,far,1,[100,150,255],THICKNESS)
            cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], THICKNESS)
    #cv2.imshow('Hull',img)
    return ["{},{}".format(cX,cY),img]

def get_ellipse(image):
    img = image.copy()
    contours = get_image_contour(image)[0]
    cnt = contours[0]
    ellipse = cv2.fitEllipse(cnt)
    cv2.ellipse(img,ellipse,(0,255,0),THICKNESS)
    (cX,cY),_,_ = cv2.fitEllipse(cnt)
    cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], -1)
    #cv2.imshow('Ellipse',img)
    return ["{},{}".format(cX,cY),img]

def get_min_enclosing_circle(image):
    img = image.copy()
    contours = get_image_contour(image)[0]
    cnt = contours[0]
    (cX,cY),radius = cv2.minEnclosingCircle(cnt)
    cv2.circle(img,(int(cX),int(cY)),int(radius),(0,255,0),THICKNESS)
    cv2.drawContours(img, [cnt], 0, (255,255,0), 4)
    cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], -1)
    #cv2.imshow('MinEnclosingCircle',img)
    return ["{},{}".format(int(cX),int(cY)),img]

def get_weighted_centroid(image):
    img = image.copy()
    img_gray = img
    if (len(img.shape) == 3):
        #print 'Converting from Color to Greyscale'
        img_gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(img_gray, BW_THRESHOLD, 255, 0)
    thresh = cv2.bitwise_not(thresh)
    rows,cols = img_gray.shape
    cX = 0.0;
    cY = 0.0;
    count = 0;
    for ii in range(rows):
        for jj in range(cols):
            k = img_gray[ii,jj]
            if(k>250):
                count += 1
                cY += ii
                cX += jj
    cX = cX/count
    cY = cY/count
    cv2.circle(img,(int(cX),int(cY)),2,[255,0,0], -1)
    #cv2.imshow('Weighted Density',img)
    return ["{},{}".format(cX,cY),img]

def get_eroded(image):
    img = image.copy()
    img_gray = img
    if (len(img.shape) == 3):
        #print 'Converting from Color to Greyscale'
        img_gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(img_gray, BW_THRESHOLD, 255, 0)
    thresh = cv2.bitwise_not(thresh)
    eroded = cv2.erode(thresh, None, iterations=3)
    contours,hierarchy = cv2.findContours(eroded,2,1)
    eroded_cnts = contours[0]
    if (len(eroded_cnts)>0):
        M = cv2.moments(eroded_cnts)
        cX = M["m10"] / M["m00"]
        cY = M["m01"] / M["m00"]
        cv2.circle(img,(int(cX),int(cY)),2,[255,0,0], -1)
        #cv2.imshow('Eroded',img)
        return ["{},{}".format(cX,cY),img]
    else:
        return [",",img]

def get_dilated(image):
    img = image.copy()
    img_gray = img
    if (len(img.shape) == 3):
        #print 'Converting from Color to Greyscale'
        img_gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(img_gray, BW_THRESHOLD, 255, 0)
    thresh = cv2.bitwise_not(thresh)
    dilated = cv2.dilate(thresh, None, iterations=3)
    contours,hierarchy = cv2.findContours(dilated,2,1)
    dilated_cnts = contours[0]
    if (len(dilated_cnts)>0):
        M = cv2.moments(dilated_cnts)
        cX = M["m10"] / M["m00"]
        cY = M["m01"] / M["m00"]
        cv2.circle(img,(int(cX),int(cY)),THICKNESS,[255,0,0], -1)
        #cv2.imshow('Dilated',img)
        return ["{},{}".format(cX,cY),img]
    else:
        return ","

directory = os.path.dirname("output/")
lowest_size = 10000.0
try:
    os.stat(directory)
except:
    print "Generating output directory"
    os.mkdir(directory)
with open('output.csv', 'w') as outputfile:
    outputfile.write("FileName,Iterations,ContoursCenterX,ContoursCenterY,BoundingBoxX,BoundingBoxY,MinAreaRectX,MinAreaRectY,HullX,HullY,EllipseX,EllipseY,WeightedDensityX,WeightedDensityY,MinEnclosingCircleX,MinEnclosingCircleY\n")
    image_list = []
    for filename in glob.glob('img/*.jpg'):
        current = cv2.imread(filename,cv2.CV_LOAD_IMAGE_COLOR)
        height, _, _ = current.shape
        if height < lowest_size:
            lowest_size = height
    print 'Smallest icon has size of {} pixels.\nAll images will be scaled to this size.'.format(lowest_size)
    for filename in glob.glob('img/*.jpg'):
        print "Processing " + filename + "..."
        name_base = os.path.basename(filename)[:-4]
        name_base = "output/"+name_base
        img = cv2.imread(filename,cv2.CV_LOAD_IMAGE_COLOR)
        img1 = resize_image(img,lowest_size)
        img1 = get_processed_contour_as_image(img1)
        cv2.imwrite(name_base+"_processed.jpg", img1)
        myRow = ""
        myRow += "{},".format(name_base)
        cont = get_contours_centroid(img1)
        _,iterations = get_image_contour(img1)
        myRow += "{},".format(iterations)
        cv2.imwrite(name_base+"_contours.jpg", cont[1])
        myRow += "{},".format(cont[0])
        bBox = get_bounding_box(img1)
        myRow += "{},".format(bBox[0])
        cv2.imwrite(name_base+"_bounding.jpg", bBox[1])
        minRect = get_min_area_rectangle(img1)
        myRow += "{},".format(minRect[0])
        cv2.imwrite(name_base+"_minAreaRect.jpg", minRect[1])
        hull = get_convex_hull(img1)
        myRow += "{},".format(hull[0])
        cv2.imwrite(name_base+"_hull.jpg", hull[1])
        ellipse = get_ellipse(img1)
        myRow += "{},".format(ellipse[0])
        cv2.imwrite(name_base+"_ellipse.jpg", ellipse[1])
        density = get_weighted_centroid(img1)
        myRow += "{},".format(density[0])
        cv2.imwrite(name_base+"_weighted.jpg", density[1])
        minCircle = get_min_enclosing_circle(img1)
        myRow += "{}".format(minCircle[0])
        cv2.imwrite(name_base+"_minCircle.jpg", minCircle[1])
        #erod = get_eroded(img1)
        #myRow += "{},".format(erod[0])
        #cv2.imwrite(name_base+"_eroded.jpg", erod[1])
        #dilat = get_dilated(img1)
        #myRow +=  "{},".format(dilat[0])
        #cv2.imwrite(name_base+"_dilated.jpg", dilat[1])
        myRow += "\n"
        outputfile.write(myRow)
