from PyQt5 import QtCore
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

from RawImage import *
from ImageThumb import *
from ThumbLoader import *

import sys, traceback

class ShapeView(QGraphicsView):
  def __init__(self, scene):
    super(ShapeView, self).__init__(scene)
    self.nbitems = 0
    self.fitInView (0, 0, 6000, 6000, Qt.KeepAspectRatio)

  def open(self, fileName):
    self.SendErrorMessage('Opening file ', fileName)
    try:
      image = RawImage(fileName)
      loader = ThumbLoader(fileName, image)
      loader.result.connect(self.addThumb)
      loader.errorMessage.connect(self.SendErrorMessage)
      loader.start()
    except MoreThanOneContour:
      self.SendErrorMessage("Error with picture " + fileName, "More than one contour.")
    except NoContour:
      self.SendErrorMessage("Error with picture " + fileName, "No contour.")
    except:
      traceback.print_exc()

  def addThumb(self, thumb):
    self.scene().addItem(thumb)
    thumb.setPos((self.nbitems % 10) * 600, (self.nbitems // 10) * 600)
    self.nbitems = self.nbitems + 1
    self.fitInView (0, 0, 6000, ((self.nbitems // 10 + 1) * 600), Qt.KeepAspectRatio)

  errorMessage = pyqtSignal(str)
  def SendErrorMessage(self, text, info = ""):
    self.errorMessage.emit("<b>" + text + "</b> " + info)

  def showErrorDialog(self, text, info = ""):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setText(text)
    msg.setInformativeText(info)
    msg.setWindowTitle("Error")
    msg.setStandardButtons(QMessageBox.Ok)
    msg.exec_()