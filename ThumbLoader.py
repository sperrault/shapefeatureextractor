from PyQt5.QtCore import *

from ImageThumb import *
from RawImage import *

import sys, traceback

class ThumbLoader(QThread):
  result = pyqtSignal(ImageThumb)
  errorMessage = pyqtSignal(str, str)

  def __init__(self, fileName, image):
    QThread.__init__(self)
    self.image = image
    self.fileName = fileName

  def __del__(self):
    self.wait()

  def run(self):
    try:
      thumb = ImageThumb(self.image)
      self.result.emit(thumb)
    except NoContour:
      self.errorMessage.emit("Error with picture " + self.fileName, "No contour.")
    except NoBlackPixel:
      self.errorMessage.emit("Error with picture '" + self.fileName + "'", "No black pixel. Please adjust the threshold")
    except NotEnoughPoints:
      self.errorMessage.emit("Error with picture '" + self.fileName + "'", "Contour has < 5 points: not enough for fitting ellipse.")
    except UnsupportedFormat:
      self.errorMessage.emit("Error opening file '" + fileName + "'", "Unsupported format")
    except:
      traceback.print_exc()