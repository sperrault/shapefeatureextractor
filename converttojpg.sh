#!/bin/bash
for i in `ls img/*.svg` ; do 
	echo $i
	# requires librsvg
	rsvg-convert -w 1024 -h 1024 $i -f png -o `basename $i .svg`.png
	# if it doesn't work on linus, try librsvg2-bin with this line:
	# rsvg -w 1024 -h 1024 $i `basename $i .svg`.png
	convert -flatten `basename $i .svg`.png `basename $i .svg`.jpg
	#rm `basename $i .svg`.png
done

